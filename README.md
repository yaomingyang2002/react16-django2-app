# React-Django

This project integrates React frontend with Django backend. A completed Django app is used to connect and integrate with React.js library step-by-step.

#### Tech
- Django 2.0.6
- Django Rest Framework 3.8.2
- Django CORS Headers 2.2.0
- React 16.4.0